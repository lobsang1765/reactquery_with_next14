import { NextRequest } from "next/server";
import jwt from "jsonwebtoken";
type typeDecode ={
    id:string,
    email:string,
    password:string
}

export const getDataFromToken = (request: NextRequest) => {
    try {
        const token = request.cookies.get("token")?.value || '';
        const decodedToken:typeDecode = jwt.verify(token, process.env.TOKEN_SECRET!);
        return decodedToken.id;
    } catch (error: any) {
        throw new Error(error.message);
    }

}