"use client"
import React, { useState } from 'react';
import { submitPost } from '@/api/getPost';
export default function FormSection() {
  const [inputValue, setInputValue] = useState("");

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();

    // Access the input value using e.currentTarget
    const formData = new FormData(e.currentTarget as HTMLFormElement);
    const title = formData.get("title") as string;
    const data = {
        id: Date.now(),
        title: title,
    };

    submitPost(data)

    // Now you can use the 'title' variable as needed
    console.log("Title:", title);
  };

  return (
    <div>
      <form className="flex flex-col" onSubmit={handleSubmit}>
        <input
          className="border mb-4 p-2"
          type="text"
          placeholder="Title"
          name="title"
          value={inputValue}
          onChange={(e) => setInputValue(e.target.value)}
        />
        <button className="border mb-4 p-2 bg-purple-500 text-white" type="submit">
          Submit
        </button>
      </form>
    </div>
  );
}
