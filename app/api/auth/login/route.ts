import { connect } from "@/dbConfig/dbConfig";
import User from "@/models/userModels";
import bcryptjs from 'bcryptjs';
import jwt from "jsonwebtoken"
import { NextResponse } from "next/server";

connect()
export async function POST (request:Request){
    try {
        const {email, password} = await request.json();

        const user = await User.findOne({email});;
        if(!user){
            return Response.json({
                error:"user doesnt existed"
            })
        }
        const validPassword = await bcryptjs.compare(password, user.password)

        if(!validPassword){
            return Response.json({
                error:"invalid password"
            })
        }

        const tokenData = {
            id:user._id,
            username:user.username,
            email:user.email
        }

        const token = await jwt.sign(tokenData, process.env.TOKEN_SECRET, { expiresIn:"1d"});

        const response = NextResponse.json({
            message:"Login successfull",
            success:true
        })

        response.cookies.set("token", token,{
            httpOnly:true
        })
        return response;
    } catch (error:any) {
        return Response.json({
            error:error.message
        })
    }
}