import { connect } from "@/dbConfig/dbConfig";
import User from "@/models/userModels";
import { NextResponse } from "next/server";
import bcryptjs from "bcryptjs"
import { sendEmail } from "@/helper/mailer";
connect();


export async function POST(request:Request) {
    try {
        const {username,email, password} = await request.json()
        console.log("username", username)

        const user = await User.findOne({email});

        if(user){
            return  NextResponse.json({error:"user already existed"})
        }

        const salt = await bcryptjs.genSalt(10);
        const hashedPassword = await bcryptjs.hash(password, salt);


        const newUser = new User({
            username,
            email,
            password:hashedPassword
        });

        const savedUser = await newUser.save();
        console.log(savedUser)

        await sendEmail({
            email,
            emailType:"VERIFY",
            userId:savedUser._id
        })
        return Response.json({
            message:"User created successfully",
            success: true,
            savedUser
        })
    } catch (error) {
        
    }
}