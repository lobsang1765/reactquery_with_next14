"use client"
import Image from "next/image";
import { useQuery } from "@tanstack/react-query";
import { getPosts } from "@/api/getPost";
import ListOfPost from "./_component/PostList";
import FormSection from "./_component/form";
import { useEffect, useState } from "react";
import axios from 'axios'
export default  function Home() {

  const [posts, setPosts] = useState([])
  const [isClient, setClient] = useState(false)
  useEffect(()=>{
    const post = async() => {
      let {data} = await axios('/api/posts/');
      console.log("client", data.data);
      setClient(true)
      setPosts(data.data)
    }
    post()
  }, [])


 
 

  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      sdsds
      <div className="p-4 flex gap-12">
        <FormSection></FormSection>
        <div className="flex-1">
          <h2 className="text-lg font-bold mb-4">Posts:</h2>
          {
            isClient && posts.length >0 && (
              <>
                {
                  posts.map((post:any) =>(

                    <ListOfPost key={post.id} post={post} />
                  )
                     
                  )
                }
              </>
            )
          }
        </div>
        
      </div>
    </main>
  );
}
