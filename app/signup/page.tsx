"use client"
import React from "react";
import Link from "next/link";
import axios from "axios";
import { useRouter } from "next/navigation";
export default function SignupPage() {
    const router = useRouter()
    const [user, setUser] = React.useState({
        email: "",
        password: "",
        username: "",
    })
    const [loading, setLoading] = React.useState(false);
    const [buttonDisabled, setButtonDisabled] = React.useState(false);
    const onSignup = async () => {
        try {
            setLoading(true)
            let response = await axios.post('/api/auth/signup', user);
            console.log("Signup success", response.data);
            router.push("/login")
        } catch (error:any) {
            console.log("Signup failed", error.message);
        }finally{
            setLoading(false)
        }
        
    }
    return (
        <div className="flex flex-col items-center justify-center min-h-screen py-2 shadow-md rounded-md">
        <h1 className="font-medium text-xl">{loading ? "Processing" : "Signup"}</h1>
        <hr />
        <div className="flex flex-col rounded-md shadow-md p-3">
        <label htmlFor="username">username:</label>
        <input 
        className="p-2 border border-gray-300 rounded-lg mb-4 focus:outline-none focus:border-gray-600 text-black"
            id="username"
            type="text"
            value={user.username}
            onChange={(e) => setUser({...user, username: e.target.value})}
            placeholder="username"
            />
        <label htmlFor="email">email:</label>
        <input 
        className="p-2 border border-gray-300 rounded-lg mb-4 focus:outline-none focus:border-gray-600 text-black"
            id="email"
            type="text"
            value={user.email}
            onChange={(e) => setUser({...user, email: e.target.value})}
            placeholder="email"
            />
        <label htmlFor="password">password:</label>
        <input 
        className="p-2 border border-gray-300 rounded-lg mb-4 focus:outline-none focus:border-gray-600 text-black"
            id="password"
            type="password"
            value={user.password}
            onChange={(e) => setUser({...user, password: e.target.value})}
            placeholder="password"
            />
            <div className="flex flex-col">
            <button
            onClick={onSignup}
            className="p-2 border border-gray-300 rounded-lg mb-4 focus:outline-none focus:border-gray-600 hover:shadow-md">
            {buttonDisabled ? "No signup" : "Signup"}
            </button>
            <Link className="text-center font-medium" href="/login">Visit login page</Link>
            </div>
        </div>
       
           
        </div>
    )
}